<!-- PROJECT LOGO -->
<br />
<p align="center">
  <h3 align="center">Mobi Mall Android</h3>

  <p align="center">
    An Android app frontend for a simple sales application
  </p>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [License](#license)
* [Contact](#contact)


## About The Project

This is the mobile app frontend built in android of a simple sales application backend built with django rest framework.
It consumes the API built by the latter.



### Built With

* []()Android
* []()Java
* []()Retrofit2



## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

* API 19+

### Installation

1. Clone the repo
```sh
git clone https://timobure@bitbucket.org/timobure/mobimallandroid.git
```
2. Build Gradle dependencies

3. Run

## License

Distributed under the MIT License. See `LICENSE` for more information.


## Contact

Timon Obure - timobure@gmail.com

Project Link: https://timobure@bitbucket.org/timobure/mobimallandroid.git