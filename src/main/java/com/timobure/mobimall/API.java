package com.timobure.mobimall;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface API {

    String BASE_URL = "https://mobimall.pythonanywhere.com/api/";

    @GET("products/")
    Call<List<Product>> getProducts();

    @GET("products/{pk}/")
    Call<Product> getProduct(@Path("pk") int pk);

    @GET("products/price/{pk}/")
    Call<ProductPrice> getProductPrice(@Path("pk") int pk);

    @GET("sales/payments/")
    Call<List<PaymentMethod>> getPaymentMethods();

    @GET("sales/payments/{pk}/")
    Call<PaymentMethod> getPaymentMethod(@Path("pk") int pk);

    @POST("sales/create/")
    @FormUrlEncoded
    Call<Sale> postSales(@Field("product") int product,
                         @Field("price") Double price,
                         @Field("payment_method") int payment_method,
                         @Field("receipt") int receipt);

    @POST("sales/receipts/")
    @FormUrlEncoded
    Call<Receipt> createNewReceipt(@Field("number") int number);

}
