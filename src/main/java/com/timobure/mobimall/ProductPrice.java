package com.timobure.mobimall;


public class ProductPrice {
    private Integer id;
    private Product product;
    private String buy_price;
    private String sell_price;
    private String load_date;

    public ProductPrice(Integer id, Product product, String buy_price, String sell_price, String load_date) {
        this.id = id;
        this.product = product;
        this.buy_price = buy_price;
        this.sell_price = sell_price;
        this.load_date = load_date;
    }

    public Integer getId() {
        return id;
    }

    public Product getProduct() {
        return product;
    }

    public String getBuy_price() {
        return buy_price;
    }

    public String getSell_price() {
        return sell_price;
    }

    public String getLoad_date() {
        return load_date;
    }
}
