package com.timobure.mobimall;

public class Receipt {
    private int number;

    public Receipt(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }
}
