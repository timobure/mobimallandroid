package com.timobure.mobimall;


public class Product {

    private Integer id;
    private String name;
    private String code;
//    private Category category;

    public Product(Integer id, String name, String code, Category category) {
        this.id = id;
        this.name = name;
        this.code = code;
//        this.category = category;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name.trim();
    }

    public String getCode() {
        return code;
    }

//    public Category getCategory() {
//        return category;
//    }
}

