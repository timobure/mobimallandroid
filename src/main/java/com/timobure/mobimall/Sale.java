package com.timobure.mobimall;

public class Sale {
    private Integer id;
    private Product product;
    private String price;
    private PaymentMethod paymentMethod;
    private Receipt receipt;

    public Sale(Integer id, Product product, String price, PaymentMethod paymentMethod, Receipt receipt) {
        this.id = id;
        this.product = product;
        this.price = price;
        this.paymentMethod = paymentMethod;
        this.receipt = receipt;
    }
    public Integer getId() {
        return id;
    }

    public Product getProduct() {
        return product;
    }

    public String getPrice() {
        return price;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public Receipt getReceipt() { return receipt; }
}
