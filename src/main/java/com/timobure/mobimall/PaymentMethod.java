package com.timobure.mobimall;

public class PaymentMethod {
    private Integer id;
    private String name;
    private String prefix;

    public PaymentMethod(Integer id, String name, String prefix) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPrefix() {
        return prefix;
    }
}
