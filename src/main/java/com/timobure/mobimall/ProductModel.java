package com.timobure.mobimall;


public class ProductModel {

    private String product;
    private double price;

    public ProductModel(String product, double price) {
        this.product = product;
        this.price = price;
    }

    public String getProduct() {
        return product;
    }

    public double getPrice() {
        return price;
    }
}
