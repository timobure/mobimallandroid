package com.timobure.mobimall;


public class Category {
    private Integer id;
    private String name;
    private String details;

    public Category(Integer id, String name, String details) {
        this.id = id;
        this.name = name;
        this.details = details;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDetails() {
        return details;
    }
}
