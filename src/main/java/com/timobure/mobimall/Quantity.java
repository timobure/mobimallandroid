package com.timobure.mobimall;

import java.util.ArrayList;

public class Quantity {
    private Integer id;
    private ArrayList<Product> product;
    private Integer quantity;

    public Quantity(Integer id, ArrayList<Product> product, Integer quantity){
        this.id = id;
        this.product = product;
        this.quantity = quantity;
    }

    public Integer getId() {
        return id;
    }

    public ArrayList<Product> getProduct() {
        return product;
    }

    public Integer getQuantity() {
        return quantity;
    }
}
