package com.timobure.mobimall;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void login(View view) {
        // Called by login button
        final EditText username = (EditText)findViewById(R.id.username);
        username.setText("Hello");
        this.startShop(view);

    }

    public void startShop(View view) {
        // Called after login is verified
        // Creates ShopActivity
//        System.out.println("Hello");
        Intent intent = new Intent(this, ShopActivity.class);
        startActivity(intent);

    }


}
