package com.timobure.mobimall;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ShopActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int PERMISSION_REQUEST_CODE = 200;
    private List<Product> products;
    private List<PaymentMethod> paymentMethods;
    private ListView listView;
    private List<String> listViewItems;
    private ArrayAdapter<String> listViewAdapter;
    private Receipt receipt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
            getProducts();
            getPaymentMethods();

            listViewItems = new ArrayList<String>();
            listView = (ListView)findViewById(R.id.list_view);
            listViewAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listViewItems);
            listView.setAdapter(listViewAdapter);

            setReceipt();

            Button btnLoad = (Button)findViewById(R.id.loadListViewBtn);
            btnLoad.setOnClickListener(this);
            Button btnSave = (Button)findViewById(R.id.saveSale);
            btnSave.setOnClickListener(this);

//            if(!isEx)
        }

    @Override
    public void onClick(View view){
        // Call action on any click
        if (view.getId() == R.id.loadListViewBtn){
            setProductId();
        }else if (view.getId() == R.id.saveSale){
            createPdfReceipt();
        }
    }

    private void setProductId(){
        // Take selected item in AutocompleteView
        // Get the product from the item String
        // Get ID from getProduct()
        // Call getProductPrice() to get the price of the product from server
        AutoCompleteTextView autocomp = (AutoCompleteTextView)findViewById(R.id.item_name);
        String productName = autocomp.getText().toString().trim();
        Product product = getProduct(productName);
        if (product != null){
            getProductPrice(product.getId());
        }
    }

    private void getProducts(){
        // Get all products from server database
        // Populate AutocompleteView using the products
        Retrofit retrofit = RetrofitClient.getRetrofitInstance();
        API productApi = retrofit.create(API.class);
        Call<List<Product>> call = productApi.getProducts();
        call.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                // the products list
                if (response.isSuccessful()){
                    products = response.body();
                    populateAutocomplete(products);
                    final AutoCompleteTextView autocomp = (AutoCompleteTextView) findViewById(R.id.item_name);
                    autocomp.setText("Ready...");
                }else{
                    try {
                        Log.e("PRODUCTS TRY ERROR", response.errorBody().string());
                    } catch (Exception e) {
                        Log.e("PRODUCTS CATCH ERROR", e.getMessage());
                    }
                }

            }
            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Log.e("GET-PRODUCTS", t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void loadProductToListview(){
        AutoCompleteTextView autocomp = (AutoCompleteTextView)findViewById(R.id.item_name);
        String productData = autocomp.getText().toString();
    }

    private void getPaymentMethods(){
        // Get payment methods from server database
        // Call populatePaymentMethods to load them into spinner
        final ArrayList<String> payments = new ArrayList<String>();
        Retrofit retrofit = RetrofitClient.getRetrofitInstance();
        API api = retrofit.create(API.class);
        Call<List<PaymentMethod>> call = api.getPaymentMethods();
        call.enqueue(new Callback<List<PaymentMethod>>() {
            @Override
            public void onResponse(Call<List<PaymentMethod>> call, Response<List<PaymentMethod>> response) {
                paymentMethods = response.body();
                for (PaymentMethod pay : paymentMethods){
                    payments.add(pay.getName());
                }
                populatePaymentMethods(payments);
            }

            @Override
            public void onFailure(Call<List<PaymentMethod>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                final AutoCompleteTextView username = (AutoCompleteTextView) findViewById(R.id.item_name);
                String msg = t.getMessage();
                username.setText(msg);
            }
        });
        Log.d("PAYMENTMETHODS", ": "+payments.size());
    }

    private void getProductPrice(int product_id){
        // Using product_id parameter
        // Get product price object from server database
        // Call addProductToList to update listview
        Retrofit retrofit = RetrofitClient.getRetrofitInstance();
        API productPriceApi = retrofit.create(API.class);
        Call<ProductPrice> call = productPriceApi.getProductPrice(product_id);
        call.enqueue(new Callback<ProductPrice>() {
            @Override
            public void onResponse(Call<ProductPrice> call, Response<ProductPrice> response) {

                ProductPrice productPrice = response.body();
                addProductToList(productPrice);
            }

            @Override
            public void onFailure(Call<ProductPrice> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                final AutoCompleteTextView username = (AutoCompleteTextView) findViewById(R.id.item_name);
                String msg = t.getMessage();
                username.setText(msg);
            }
        });
    }

    private void postSales(int product, Double price, int payment_method){
        Retrofit retrofit = RetrofitClient.getRetrofitInstance();
        API api = retrofit.create(API.class);

        Call<Sale> call = api.postSales(product, price, payment_method, receipt.getNumber());
        call.enqueue(new Callback<Sale>() {
            @Override
            public void onResponse(Call<Sale> call, Response<Sale> response) {
                if(response.isSuccessful()){
                    // Sale sale = response.body();
                    Log.i("POST-SALES", "Success");
                }else{
                    try {
                        Log.e("POST-SALES", "Response T: "+response.errorBody().string());
                    } catch (Exception e) {
                        Log.e("POST-SALES", "Response C: "+e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<Sale> call, Throwable t) {
                Log.e("POST-SALES", "Failure: "+t.getMessage());
            }
        });

    }

    private Product getProduct(String name){
        for (Product prod : products){
            if (prod.getName().equals(name)){
                return prod;
            }
        }
        return null;
    }

    private PaymentMethod getPaymentMethod(String pm_name){
        for (PaymentMethod pm : paymentMethods){
            if(pm.getName().equals(pm_name)){
                return pm;
            }
        }
        return null;
    }

    private void populateAutocomplete(List<Product> ps){
        // Using List of product objects
        // Create new list using name and Id
        // Attach list to Autocomplete
        ArrayList<String> arrayListOfProducts = new ArrayList<>();
        for (Product p : ps){
            String product = p.getName();
            arrayListOfProducts.add(product);
        }
        String[] listOfProducts = new String[arrayListOfProducts.size()];
        listOfProducts = arrayListOfProducts.toArray(listOfProducts);
        ArrayAdapter<String> autoAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, listOfProducts);
        AutoCompleteTextView autocomp = (AutoCompleteTextView)findViewById(R.id.item_name);
        autocomp.setAdapter(autoAdapter);
        autocomp.setThreshold(1);
        autocomp.setSelectAllOnFocus(true);
    }

    private void populatePaymentMethods(ArrayList<String> payments){
        // Using Arraylist of payments
        // Load payments to payments spinner
        Spinner spinner = (Spinner)findViewById(R.id.paymentSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, payments);
        spinner.setAdapter(adapter);
    }

    private void addProductToList(ProductPrice productPrice){
        // Using productPrice
        // Create String of name and price
        // Load to listview
        // Increase total price by product price
        // Set focus for easy typing
        String format = "%-40s%s%n";
        String product = String.format(format,  productPrice.getProduct().getName()+"\t\t-",
                                                productPrice.getSell_price());
        listViewItems.add(product);
        listViewAdapter.notifyDataSetChanged();
        final AutoCompleteTextView productName = (AutoCompleteTextView) findViewById(R.id.item_name);
        productName.clearFocus();
        productName.requestFocus();
        final TextView totalView = (TextView)findViewById(R.id.totalView);
        Double totalPrice = Double.parseDouble(totalView.getText().toString());
        totalPrice = totalPrice + Double.parseDouble(productPrice.getSell_price());
        totalView.setText(totalPrice.toString());

    }

    private String balance_length(String s){
        int bal = 20 - s.length();
        return s + new String(new char[bal]).replace("\0", " ");
    }

    private void setReceipt(){
        Retrofit retrofit = RetrofitClient.getRetrofitInstance();
        API api = retrofit.create(API.class);

        Call<Receipt> call = api.createNewReceipt(1);
        call.enqueue(new Callback<Receipt>() {
            @Override
            public void onResponse(Call<Receipt> call, Response<Receipt> response) {
                if(response.isSuccessful()){
                     receipt = response.body();
                    Log.i("RECEIPT", "Success");
                }else{
                    try {
                        Log.e("RECEIPT", "Response T: "+response.errorBody().string());
                    } catch (Exception e) {
                        Log.e("RECEIPT", "Response C: "+e.getMessage());
                    }
                }
            }
            @Override
            public void onFailure(Call<Receipt> call, Throwable t) {
                Log.e("POST-SALES", "Failure: "+t.getMessage());
            }
        });
    }

    private void createPdfReceipt(){
        // Get all listview items
        // Create a pdf receipt
        // Save and run the receipt
        checkForStoragePermission();
        ListView listView = (ListView)findViewById(R.id.list_view);

        String[] itemsArray = new String[listView.getCount()];

        for (int i=0; i < listView.getCount(); i++){
            itemsArray[i] = listView.getItemAtPosition(i).toString();
            Log.i("RECEIPT ITEM", itemsArray[i]);
        }
        Spinner spinner = (Spinner)findViewById(R.id.paymentSpinner);
        String price = spinner.getSelectedItem().toString();

        // send results for POST
        extractProductForSale(itemsArray, price);

        PdfDocument document = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(300, 600, 1).create();

        PdfDocument.Page page = document.startPage(pageInfo);
        Canvas canvas = page.getCanvas();
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        canvas.drawText("Mobi Mall 2", 110, 50, paint);
        canvas.drawText("Your Mall in your Palm", 80, 110, paint);

        int y = 150;
        for (String item: itemsArray){
            canvas.drawText(item, 30, y, paint);
            y = y+15;
        }
        // Set Total Text
        TextView totalView = (TextView)findViewById(R.id.totalView);
        String total = totalView.getText().toString();
        String format = "%-50s%s%n";
        String totalText = String.format(format, "Total", total);
        canvas.drawText(totalText, 30, y, paint);



        document.finishPage(page);

        String dirpath = Environment.getExternalStorageDirectory().getPath();


        File dir = new File(dirpath+"/Mobimall");
        Log.i("DIR PATH", dirpath);

        if (!dir.exists()) {
            dir.mkdir();
        }
        Log.d("File Path New", dir.getAbsolutePath());

        String file = dir+"/receipt.pdf";
        File filepath = new File(file);

        try{
            document.writeTo(new FileOutputStream(filepath));
            document.close();
            Toast.makeText(this, "Done", Toast.LENGTH_LONG).show();
            resetSales();
        }catch (IOException e){
            Log.e("PDFERROR", e.toString());
            Toast.makeText(this, "Error: "+e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    private void checkForStoragePermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                        ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_CODE);
        }
    }

    private void resetSales(){
        setReceipt();
        TextView total = (TextView)findViewById(R.id.totalView);
        ListView listView = (ListView)findViewById(R.id.list_view);
        AutoCompleteTextView autocomp = (AutoCompleteTextView)findViewById(R.id.item_name);
        listViewItems.clear();
        listViewAdapter.notifyDataSetChanged();
        total.setText("0.00");
        autocomp.setText("Ready...");
        autocomp.clearFocus();
        autocomp.requestFocus();


    }

    private String[] extractProductForSale(String[] strings, String payMethodName){
        PaymentMethod pm = getPaymentMethod(payMethodName);
        String [] prodStr = new String[2];

        for (String s : strings) {
            prodStr[0] = s.split("-")[0].trim();
            prodStr[1] = s.split("-")[1].trim();

            Product product = getProduct(prodStr[0]);
            int productID = product.getId();
            double price = Double.parseDouble(prodStr[1]);
            price = Math.round(price);
            price = Math.round(price);
            Log.i("PRODUCT Pk", ""+product);
            Log.i("PRODUCT Price", ""+price);
            postSales(productID, price, pm.getId());
        }
        return prodStr;
    }
}
